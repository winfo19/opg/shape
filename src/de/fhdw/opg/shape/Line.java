package de.fhdw.opg.shape;

public class Line extends Shape {
    private double length;

    public Line(double length) {
        this.length = length;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public double getExpansion() {
        return length;
    }

    @Override
    public double getAreaSize() {
        return 0;
    }

    @Override
    public String toString() {
        return "Line{" + "length=" + length + '}';
    }
}
