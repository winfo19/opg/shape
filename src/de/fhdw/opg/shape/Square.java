package de.fhdw.opg.shape;

public class Square extends Rectangle {
    public Square(double length) {
        super(length, length);
    }

    @Override
    public String toString() {
        return "Square{" + "length=" + length + '}';
    }
}
