package de.fhdw.opg.shape;

public abstract class Shape {
    public abstract double getLength();

    public abstract double getHeight();

    public abstract double getExpansion();

    public abstract double getAreaSize();

    @Override
    public String toString() {
        return "Shape{}";
    }
}
