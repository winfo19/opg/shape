package de.fhdw.opg.shape;

public class Point extends Shape {
    @Override
    public double getLength() {
        return 0d;
    }

    @Override
    public double getHeight() {
        return 0d;
    }

    @Override
    public double getExpansion() {
        return 0;
    }

    @Override
    public double getAreaSize() {
        return 0d;
    }

    @Override
    public String toString() {
        return "Point{}";
    }
}
