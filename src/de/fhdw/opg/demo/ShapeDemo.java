package de.fhdw.opg.demo;

import de.fhdw.opg.shape.*;

public class ShapeDemo {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[]{
                new Point(),
                new Line(7.7),
                new Square(6d),
                new Ellipse(3d, 2d),
                new Circle(5.6)
        };

        for (Shape shape : shapes) {
            System.out.println(shape
                               + ": length = "
                               + shape.getLength()
                               + ", height = "
                               + shape.getHeight()
                               + ", expansion = "
                               + shape.getExpansion()
                               + ", area size = "
                               + shape.getAreaSize());
        }
    }
}
