package de.fhdw.opg.demo;

import de.fhdw.opg.drawable.*;

public class DrawableDemo {
    public static void main(String[] args) {
        Drawable[] drawable = {
            new Point(),
            new Line(7.5),
            new Rectangle(5d, 9.9),
            new Square(5.6)
        };

        printDrawables(drawable);
    }

    public static void printDrawables(Drawable[] drawables) {
        for (Drawable drawable : drawables) {
            System.out.println(
                    drawable
                    + ": length = " + drawable.getLength()
                    + " , height = " + drawable.getHeight()
                    + " , expansion = " + drawable.getExpansion()
                    + " , area size = " + drawable.getAreaSize()
            );
        }
    }
}
