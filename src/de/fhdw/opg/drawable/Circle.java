package de.fhdw.opg.drawable;

public class Circle extends Ellipse {
    public Circle(double radius) {
        super(radius, radius);
    }

    @Override
    public String toString() {
        return "Ellipse{" + "radius=" + semiHorizontalAxis + '}';
    }
}
