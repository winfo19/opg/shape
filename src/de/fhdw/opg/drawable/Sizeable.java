package de.fhdw.opg.drawable;

public interface Sizeable {
    double getAreaSize();
}
