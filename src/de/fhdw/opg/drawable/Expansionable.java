package de.fhdw.opg.drawable;

public interface Expansionable {
    double getExpansion();
}
