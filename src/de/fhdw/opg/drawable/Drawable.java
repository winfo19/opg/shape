package de.fhdw.opg.drawable;

public interface Drawable extends Expansionable, Sizeable {
    double getLength();

    double getHeight();

    default double getExpansion() {
        return Math.max(getLength(), getHeight());
    }
}
