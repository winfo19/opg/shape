package de.fhdw.opg.drawable;

public class Rectangle implements Drawable {
    protected double length;
    protected double height;

    public Rectangle(double length, double height) {
        this.length = length;
        this.height = height;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public double getAreaSize() {
        return length * height;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "length=" + length + ", height=" + height + '}';
    }
}
