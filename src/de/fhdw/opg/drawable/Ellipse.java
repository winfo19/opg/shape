package de.fhdw.opg.drawable;

public class Ellipse implements Drawable {
    protected double semiHorizontalAxis;
    protected double semiVerticalAxis;

    public Ellipse(double semiHorizontalAxis, double semiVerticalAxis) {
        this.semiHorizontalAxis = semiHorizontalAxis;
        this.semiVerticalAxis = semiVerticalAxis;
    }

    @Override
    public double getLength() {
        return 2d * semiHorizontalAxis;
    }

    @Override
    public double getHeight() {
        return 2d * semiVerticalAxis;
    }

    @Override
    public double getAreaSize() {
        return Math.PI * semiHorizontalAxis * semiVerticalAxis;
    }

    public double getSemiHorizontalAxis() {
        return semiHorizontalAxis;
    }

    public double getSemiVerticalAxis() {
        return semiVerticalAxis;
    }

    @Override
    public String toString() {
        return "Ellipse{" + "semiHorizontalAxis=" + semiHorizontalAxis + ", semiVerticalAxis=" + semiVerticalAxis + '}';
    }
}
