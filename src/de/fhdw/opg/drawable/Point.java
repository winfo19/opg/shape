package de.fhdw.opg.drawable;

public class Point implements Drawable {
    @Override
    public double getLength() {
        return 0d;
    }

    @Override
    public double getHeight() {
        return 0d;
    }

    @Override
    public double getExpansion() {
        return 0;
    }

    @Override
    public double getAreaSize() {
        return 0d;
    }

    @Override
    public String toString() {
        return "Point{}";
    }
}
